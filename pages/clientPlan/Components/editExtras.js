import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";



export default function ModalEditExtras(props) {
    const { row, type, price, detail, reloadExtras, setReloadExtras, client_plan_id} = props;
    const [isLoading, setIsLoading] = useState(false)
    const [showEditModal, setShowEditModal] = useState(false);
    const [typeForm, setTypeForm] = useState(type)
    const [detailsForm, setDetailsForm] = useState(detail)
    const [priceForm, setPriceForm] = useState(price)


    const actualizarDatos = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        data.append("type", typeForm);
        data.append("detail", detailsForm);
        data.append("price", priceForm);
        
        if(formData.mediaUrl.length !== 0){
            data.append("media_url", formData.mediaUrl[0]);
        }
    
        fetch("http://slogan.com.bo/roadie/clientsPlansExtras/editMobile/" + row.id , {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            console.log('VALOR ENDPOINTS EDIT: ', data);
            setIsLoading(false)
            if (data.status) { 
                setReloadExtras(!reloadExtras)
                setShowEditModal(false)
                console.log('edit endpoint: ' + data.status);
            } else {
                console.error(data.error)
            }
        })
    
      }

    const validationSchema = Yup.object().shape({
        type: Yup.string()
            .required('Is required'),
            //.matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        detail: Yup.string()
            .required('Is required'),
            //.matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        price: Yup.string()
            .required('Is required')
            //.matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data:');
        console.log(data);
        actualizarDatos(data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)} >

                <div className="mt-[20px] grid grid-cols-12 gap-4">

                    <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Type</p>
                        <select
                            {...register('type')}
                            name="type"
                            value={typeForm}
                            onChange={(e) => {
                                setTypeForm(e.target.value)
                            }}
                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                        >
                            <option value="Image">Image</option>
                            <option value="Album">Album</option>
                            <option value="Video">Video</option>
                            <option value="Story">Story</option>
                            <option value="Reel">Reel</option>
                        </select>
                        <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                    </div>
                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Details</p>
                        <input name="detailForm"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                        {...register('detail')}
                        value={detailsForm}
                        onChange={(e) => {
                            setDetailsForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.detailsForm?.message}</div>
                    </div>
                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Image</p>
                        <input 
                            type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px]'
                        {...register('mediaUrl')}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                    </div>
                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Price</p>
                        <input name='price' type="number" placeholder="1.0" step="0.01" min="0.01"  className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                        {...register('price')}
                        value={priceForm}
                        onChange={(e) => {
                            setPriceForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.price?.message}</div>
                    </div>                                           
                </div>

                <div className="flex flex-row justify-between mt-[20px]">

                    <div>
                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                    </div>

                    <div>
                        <button
                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                            onClick={() =>
                                setShowEditModal(false)
                            }
                            disabled={isLoading}
                        >
                            Cancel
                        </button>
                        <button
                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                            type="submit"
                            disabled={isLoading}
                        >
                            {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
}