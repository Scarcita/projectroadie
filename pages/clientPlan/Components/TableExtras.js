import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import TooltipPost from './tooltipPost';
import TooltipExtras from './tooltipExtras';

const TableExtras = (props) => {

    const {reloadExtras, setReloadExtras, client_plan_id} = props;

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const headlist = [
        'Type', "Details", "Price", "Media Url", "Status"
    ];

    useEffect(() => {
        
        console.warn('reloaddd  ' + reloadExtras);

        setIsLoading(true)

        fetch('https://slogan.com.bo/roadie/clientsPlansExtras/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    setData(data.data)
                    console.log(data.data);
                } else {
                    console.error('Errors: ' + data.errors)
                }
                setIsLoading(false)
            })

    }, [reloadExtras])

    
    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaExtraProduct data={data} headlist={headlist}  client_plan_id={client_plan_id } reloadExtras={reloadExtras} setReloadExtras={setReloadExtras} />
                    <TablaResponsive headlist={headlist} data={data} client_plan_id={client_plan_id } reloadExtras={reloadExtras} setReloadExtras={setReloadExtras}  />
                </div>
            </>
    )
}


const TablaExtraProduct = (props) => {
    const { headlist, data, reloadExtras, setReloadExtras } = props;
    return (

        <div className="rounded-lg shadow hidden lg:block md:block mt-[20px]">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map((header, index) => <th key={index} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                        {
                            const imgType = "";

                            if(row !== undefined){
                                if (row !== null){
                                  if(row.type !== null && row.type !== undefined){
                                    switch (row.type) {
                                      case 'Image': 
                                        imgType = '/Board/image.png'         
                                          break;
                                      case 'Album':
                                        imgType= '/Board/image.png'
                                          break;
                                      case 'Video':
                                        imgType = '/Board/video.png'
                                      
                                      break;
                                      case 'Story':
                                        imgType = '/Board/video.png'
                                      
                                      break;
                                      case 'Reel':
                                        imgType = '/Board/video.png'
                                      
                                      break;
                                          
                                      default:
                                          break;
                                  }
                                  }
                                }
                            }
                            
                        return (
                        <tr key={row.id}>
                            <td className='text-[12px] text-center'>
                                <Image
                                    className=''
                                    src={imgType}
                                    alt='img'
                                    layout='fixed'
                                    width={40}
                                    height={40}
                                />
                            </td>

                            <td>
                                <p className='text-[12px] text-left leading-2 whitespace-normal'>
                                    {row.detail}
                                </p>

                            </td>
                            <td className='text-center'>
                                <p className='text-[12px] leading-2 whitespace-normal'>
                                    {row.price} USD
                                </p>

                            </td>
                            <td>
                                <div className='items-center text-center'>
                                    {row.media_url !== null && row.media_url.trim().length !== 0 ?
                                        <Image
                                            className='rounded-md'
                                            src={row.media_url}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                </div>
                            </td>
                            <td className=''>
                                <p className='bg-[#D9D9D9] rounded-[6px] text-[12px] text-center items-center self-center pt-[2px] pb-[2px] pl-[3px] pr-[3px]'>
                                    {row.status}
                                </p>
                            </td>
                            <td className='items-center text-center '>
                                <TooltipExtras
                                    row={row}
                                    type={row.type}
                                    price={row.price}
                                    detail={row.detail}
                                    reloadExtras={reloadExtras}
                                    setReloadExtras={setReloadExtras}
                                
                                />

                            </td>
                        </tr>
                            )

                        }
                    )}
                </tbody>
            </table>
        </div>
    );
};

const TablaResponsive = (props) => {
    const { headlist, data, reloadExtras, setReloadExtras  } = props;

    return (
        <div className='grid grid-cols-12 mt-[20px]'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                        {
                            const imgType = "";

                            if(row !== undefined){
                                if (row !== null){
                                  if(row.type !== null && row.type !== undefined){
                                    switch (row.type) {
                                      case 'Image': 
                                        imgType = '/Board/image.png'         
                                          break;
                                      case 'Album':
                                        imgType= '/Board/image.png'
                                          break;
                                      case 'Video':
                                        imgType = '/Board/video.png'
                                      
                                      break;
                                      case 'Story':
                                        imgType = '/Board/video.png'
                                      
                                      break;
                                      case 'Reel':
                                        imgType = '/Board/video.png'
                                      
                                      break;
                                          
                                      default:
                                          break;
                                  }
                                  }
                                }
                            }

                        return (
                            <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                                <div className='col-span-12 md:col-span-12'>
                                    <div className='grid grid-cols-12'>
                                        <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                            <div className='grid grid-cols-12 gap-3'>
                                                <div className='col-span-7 md:col-span-7'>
                                                    <div className=''>
                                                        <p className='text-[14px] text-left font-medium leading-2 whitespace-normal'>
                                                            {row.detail}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div className='col-span-5 md:col-span-5 text-end'>
                                                    <p className='bg-[#D9D9D9] rounded-[6px] text-[12px] text-center items-center self-center pt-[2px] pb-[2px] pl-[3px] pr-[3px]'>
                                                        {row.status}
                                                    </p>
                                                </div>
                                            </div>
                                           
                                            <div className='grid grid-cols-12 gap-3 mt-[5px]'>
                                                <div className='col-span-6 md:col-span-6 text-center'>
                                                    
                                                <Image
                                                    className=''
                                                    src={imgType}
                                                    alt='img'
                                                    layout='fixed'
                                                    width={40}
                                                    height={40}
                                                />
                                    
                                                </div>
                                                <div className='col-span-6 md:col-span-6 text-end'>
                                                    {row.media_url !== null && row.media_url.trim().length !== 0 ?
                                                    <Image
                                                        className='rounded-md'
                                                        src={row.media_url}
                                                        alt=''
                                                        layout='fixed'
                                                        width={40}
                                                        height={40}
                                                    />
                                                    : <></>
                                                    }
                                    
                                                </div>
                                                
                                            </div>
                                            <div className='grid grid-cols-12 '>
                                                <div className='col-span-6 md:col-span-6 text-center self-center'>
                                                
                                                    <div className=' text-[18px] font-semibold'>
                                                        {row.price} USD
                                                    </div>
                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-span-2 md:col-span-12'>
                                            <div className=' items-center text-center '>
                                            <TooltipExtras
                                                row={row}
                                                type={row.type}
                                                price={row.price}
                                                detail={row.detail}
                                                reloadExtras={reloadExtras}
                                                setReloadExtras={setReloadExtras}    
                                            />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            )
                        }
                    )}
            </div>
        </div>

        
    );
};




export default TableExtras;