import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';
import Image from 'next/image';


export default function ModalAds(props) {
    
    const { reloadAds, setReloadAds, client_plan_id} = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(true)

    const [subTitle, setSubTitle] = useState("");
    const [title, setTitle] = useState('')
    const [post, setPost] = useState('')
    const [rrss, setRrss] = useState('')
    const [type, setType] = useState('')

    const [clientPlanPostId, setClientPlanPostId] = useState(null)
    const [startDate, setStartDate] = useState('')
    const [startTime, setStartTime] = useState('')
    const [endDate, setEndDate] = useState('')
    const [endTime, setEndTime] = useState('')
    const [amount, setAmount] = useState('')

    const [clientPlanPosts, setClientPlanPosts] = useState([])

    const clearForm = () => {
        setStartDate('')
        setEndDate('')
        setAmount('')
    }

    const formatOptionLabel = ({ value, label, post }) => {
        
        const imgUrl = "";

        if(post.social_network !== null){
            switch (post.social_network) {
                case 'Facebook': 
                    imgUrl = '/SocialMedia/Facebook.svg'
                    break;
                case 'TikTok':
                    imgUrl= '/SocialMedia/TikTok.svg'
                    break;

                case 'Instagram':
                    imgUrl = '/SocialMedia/Instagram.svg'
                break;
                case 'YouTube':
                    imgUrl = '/SocialMedia/Youtube.svg'
                break;
                case 'Mailing':
                    imgUrl = '/Plans/gmail.svg'
                break;
                case 'LinkedIn':
                    imgUrl = '/SocialMedia/messenger.svg'
                break;
                case 'Twitter':
                    imgUrl = '/SocialMedia/Twitter.svg'
                break;
                    
                default:
                    break;
            }
        }

        const hasImage = false;
        if(post.media_url !== null && post.media_url.trim().length !== 0){
            hasImage = true;
        }

        return (
            <div className='grid grid-cols-12 align-center'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row mt-[5px] mb-[5px]'>
                    {hasImage ?
                        <div className='pr-[10px]'>
                            <Image
                                className='rounded-md'
                                src={post.media_url}
                                alt=''
                                layout='fixed'
                                width={40}
                                height={40}
                            />
                        </div>
                        : <></>
                    }
                    {hasImage ?
                        <div className='pr-[10px]'>
                            <Image
                                className='rounded-md'
                                src={imgUrl}
                                alt=''
                                layout='fixed'
                                width={40}
                                height={40}
                            />
                        </div>
                        : <></>
                    }
                <div className={ hasImage ? 'col-span-8' : 'col-span-12' + ' text-[14px]'}>
                    {post.title}
                    <div>
                        <small className='text-gray-500 text-[12px]'>{post.subtitle}</small>
                    </div>
                </div>
                </div>
            </div>
        )
    };      
    
    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.title, post: result})
                })

                setClientPlanPosts(temp)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

    }, [])
    
    const getDataOrden = async () => {

        var data = new FormData();
    
        data.append("client_plan_post_id", clientPlanPostId.value);
        data.append("start_datetime", startDate + ' ' + startTime);
        data.append("end_datetime", endDate + ' ' + endTime );
        data.append("amount", amount);
    
        fetch("https://slogan.com.bo/roadie/clientsPlansPostsAds/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            //console.log('VALOR ENDPOINTS: ', data);

            setIsLoading(false)
            if(data.status){
              clearForm()
              setReloadAds(!reloadAds)
              setShowModal(false)
            } else {
              alert(JSON.stringify(data.errors, null, 4))
            }
            
          },
          (error) => {
            console.log(error)
          }
          )
    
      }
    
    
    const validationSchema = Yup.object().shape({
            
        startDate: Yup.string()
            .required('startDate is required'),
    
        startTime: Yup.string()
        .required('startTime is required'),
    
        endTime: Yup.string()
            .required('endTime is required'),

        endDate: Yup.string()
        .required('endDate is required'),

        amount: Yup.string()
        .required('amount is required'),
    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            getDataOrden()
            return false;
            
        }

    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[26px] md:w-[200px] md:h-[48px] lg:w-[160px] lg:h-[35px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[14px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Ads
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE ADS
                                    </h4>

                                    </div>
                                    <div>
                                        <form onSubmit={handleSubmit(onSubmit)}>

                                            <div className="mt-[20px] grid grid-cols-12 gap-4">

                                                <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Post</p>
                                                    <ReactSelect 
                                                        defaultValue={clientPlanPostId}
                                                        onChange={setClientPlanPostId}
                                                        formatOptionLabel={formatOptionLabel}
                                                        options={clientPlanPosts} 
                                                    />
                                                </div>

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date start </p>
                                                    <input name="startDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                    {...register('startDate')}
                                                    value={startDate}
                                                    onChange={(e) => {
                                                        setStartDate(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.startDate?.message}</div>
                                                </div>
                                                
                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time Star</p>
                                                    <input name="startTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                    {...register('startTime')}
                                                    value={startTime}
                                                    onChange={(e) => {
                                                        setStartTime(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.startTime?.message}</div>
                                                </div>

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date End </p>
                                                    <input name="endDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                    {...register('endDate')}
                                                    value={endDate}
                                                    onChange={(e) => {
                                                        setEndDate(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.endDate?.message}</div>
                                                </div>
                                                
                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time End</p>
                                                    <input name="endTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                    {...register('endTime')}
                                                    value={endTime}
                                                    onChange={(e) => {
                                                        setEndTime(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.endTime?.message}</div>
                                                </div>

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>amount</p>
                                                    <input name='amount' type="number" placeholder="1.0" step="0.01" min="0.01"  className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                    {...register('amount')}
                                                    value={amount}
                                                    onChange={(e) => {
                                                        setAmount(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.amount?.message}</div>
                                                </div>
                                            
                                            </div>
                                            <div className="flex flex-row justify-between mt-[20px]">

                                                <div>
                                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                                </div>

                                                <div>
                                                    <button
                                                        className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                        onClick={() =>
                                                            setShowModal(false)
                                                        }
                                                        disabled={isLoading}
                                                    >
                                                        Cancel
                                                    </button>
                                                    <button
                                                        className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                        type="submit"
                                                        disabled={isLoading}
                                                    >
                                                        {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}