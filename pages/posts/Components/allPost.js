import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import ImgPublic from '../../../public/Calendar/public.svg'
import Image from 'next/image'
import Layout from '../../components/layout';
import { useEffect, useState } from 'react'



export default function AllPosts(props) {

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    //const [post, setPost] = useState([]);
    const [singlePost, setSinglePost] = useState([]);

    const [postsOnly, setPostsOnly] = useState([]);
    var postsTemp = [];
    
    useEffect(() => {
      fetch('https://slogan.com.bo/roadie/clientsPlansPosts/monthCalendar')
      .then(response => response.json())
      .then(data => {
          if (data.status) {
              //console.log('ENDPOINT CALENDAR:', Object.values(data.data));
              setData(Object.values(data.data))

              Object.values(data.data).map((date) => {

                date.map((result) =>{
                  postsTemp.push(result);
                   console.log('VIEW RESULT: ' + result);
                })

              })

              //console.log("Posts Temp"+Object.entries(postsTemp[0]));
              setPostsOnly(postsTemp);

          } else {
              console.error(data.error)
          }
          setIsLoading(false)
      })
  }, [])


    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
            <div >
            <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                <div className='grid grid-cols-12'>
                  <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                      <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>All Posts</div> 
                  </div>
                  
                </div >
                {
                postsOnly.map((post, index) => {
                  
                  if(post.planned_datetime !== null && post.planned_datetime !== undefined){
                    var eventdate = post.planned_datetime;
                    var splitdate = eventdate.split('-');
                    //console.log(splitdate);
                    var day = splitdate[2];
                    var year = splitdate[0];
                  }
            
                  if(post.planned_datetime !== null && post.planned_datetime !== undefined){
                    var months = ['Jan.', 'Feb.', 'Mar. ', 'Apr.', 'May.', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                    var m = new Date(post.planned_datetime);
                    var monthName = months[m.getMonth()];
                  }

                  if(index !== 'POSTED') {
                    return <></>
                  }

                  return (
                    
                    <div className='grid grid-cols-12 mt-[30px]'
                    key={post.id}>
                      <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                        <div className='grid grid-cols-12'> 

                        <div className='col-span-6 md:col-span-6 lg:col-span-3 bg-[#FFFFFF] shadow-md rounded-[20px] p-[20px] '>
                          <div className="flex flex-row">
                            <div className="w-[48px] h-[48px] rounded-full bg-[#643DCE] self-center items-center">
                              {post.clients_plan.client.img_url != null && post.clients_plan.client.img_url.length > 0 ?
                            
                                <Image 
                                  className='rounded-full'
                                  src={post.clients_plan.client.img_url}
                                  alt='media'
                                  height={17}
                                  width={17}>

                                </Image> 
                                : <></>  
                              } 

                            </div>
                            <div className="pl-[10px]">
                              {post.clients_plan.client.name !== null ?
                                <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal'>
                                  {post.clients_plan.client.name}
                                </p>
                                : <></>
                              }
                              <p className='text-[12px]'>{monthName}{day}, {year}</p>

                            </div>

                          </div>
                          <div className='mt-[15px]'>
                            {post.media_url != null && post.media_url > 0 ?
                              
                              <Image
                              className='rounded-full'
                              src={post.media_url}
                              alt='media'
                              layout='responsive'
                              // height={37}
                              // width={37}
                              >
                            </Image>
                              : <></>  
                            } 
                          </div>
                          
                            {post.post_copy !== null && post.post_copy !== undefined ?
                            <div className='mt-[5px] '>
                              <p className='text-[12px] leading-4 whitespace-normal'>{post.post_copy}</p>
                              
                            </div>
                            : <></>
                            }
                             <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'> 

                              <button className='w-full h-[48px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[18px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7]'>
                                
                                Edit post

                              </button>
                                
                            </div>

                        </div>
                        
                        </div >
                      </div>
                    </div>
                )})}
            </div>
          </div >
        </div>
            </>
    )
}

AllPosts.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}
