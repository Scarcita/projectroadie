import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';

export default function SelectClient(props) {


    const [showModal, setShowModal] = useState(false);

    const [isLoadingClients, setIsLoadingClients] = useState(true);
    const [isLoadingPlans, setIsLoadingPlans] = useState(true);

    const [showPlansSelect, setShowPlansSelect] = useState(false);
    const [showFormFields, setShowFormFields] = useState(false);

    const { reloadClients, setReloadClients, client_id, getPostsByClientPlan } = props;

    const [time, setTime] = useState('')

    const [selectedClient, setSelectedClient] = useState(null);
    const [clients, setClients] = useState([]);

    const [selectedPlan, setSelectedPlan] = useState(null);
    const [plans, setPlans] = useState([]);

    const [socialNetwork, setSocialNetwork] = useState("");
    const [type, setType] = useState('a')
    const [plannedDateTime, setPlannedDateTime] = useState('')
    const [title, setTitle] = useState('')
    const [subtitle, setSubTitle] = useState('')
    const [instructions, setInstructions] = useState('')
    const [postCopy, setPostCopy] = useState('')
    const [mediaUrl, setMediaUrl] = useState('')

    const clearForm = () => {
        setSocialNetwork('')
        setType('')
        setPlannedDateTime('')
        setTitle('')
        setSubTitle('')
        setInstructions('')
        setPostCopy('')
        setMediaUrl('')
    }

    /////////////////////// SELECT NAME ///////////////////////

    const formatOptionLabel1 = ({ value, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>

                <div>
                    <small className='text-black text-xs'>{post.name}</small>
                </div>
            </div>
        )
    }; 

    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clients/all')
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.name, post: result})
                })

                setClients(temp)
                //console.log('DATA CLIENTS: ', temp);
                
            } else {
                console.error(data.error)
            }
            setIsLoadingClients(false)
        })

    }, [])


    /////////////////////////////////SELECT PLAN///////////////////////////////////////

    const formatOptionLabel = ({ row, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>

                <div>
                    <small className='text-gray-500 text-xs'>{post.plan.name}</small>
                </div>
            </div>
        )
    }; 

    useEffect(() => {
        if(selectedClient){
            setIsLoadingPlans(true)
            setShowPlansSelect(true)
            fetch('https://slogan.com.bo/roadie/clientsPlans/all/' + selectedClient.value)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
    
                    var temp = [];
                    data.data.map((row) => {
                        temp.push({value: row.id, label: row.plan.name, post: row})
                    })
    
                    setPlans(temp)
                } else {
                    console.error(data.error)
                }
                setIsLoadingPlans(false)
            })
        }
    }, [selectedClient])


    useEffect(() => {
        if(selectedPlan){
            console.warn(selectedPlan)
            getPostsByClientPlan(selectedPlan.value)
            setShowFormFields(true)
        }
    }, [selectedPlan])


   
    const getDataOrden = async () => {
        var data = new FormData();
        data.append("client_id", selectedClient.value);
        data.append("client_plan_id", selectedPlan.value);
        data.append("title", title);
        data.append("social_network", socialNetwork);
        data.append("type", type);
        data.append("planned_datetime", plannedDateTime);
        data.append("instructions", instructions);
        data.append("post_copy", postCopy);
        data.append("media_url", mediaUrl);

    
        fetch("http://slogan.com.bo/roadie/clientsPlansPosts/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
              //console.log('VALOR ENDPOINTS: ', data);

              if(data.status){
                clearForm()
                //setReloadClients(!reloadClients)
                //setReloadPosts(!reloadPosts)
                setShowModal(false)
                //IMPLEMENTAR RECARGA DE DATOS (COMPARTIR FUNCION DE PANTALLA AL MODAL)
              }
              
            },
            (error) => {
              //console.log(error)
            }
          )
    
      }
    

          
    const validationSchema = Yup.object().shape({
        // clientPlanId: Yup.string()
        // .required('clientPlanId is required'),
    
        type: Yup.string()
            .required('type is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        socialNetwork: Yup.string()
            .required('socialNetwork is required')
            .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        type: Yup.string()
            .required('type is required'),
        //   //.min(6, 'minimo 6 caracteres'),
        // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),
    
        plannedDateTime: Yup.string()
            .required('plannedDateTime is required'),
    
        instructions: Yup.string()
            .required('instructions is required'),
    
        postCopy: Yup.string()
            .required('postCopy is required'),

        time: Yup.string()
        .required('time is required'),

    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            //console.log('string is NOT empty')
            getDataOrden()
            return false;
            
        }

    return (

                <div>
                    <form onSubmit={handleSubmit(onSubmit)}>

                        <div className=" grid grid-cols-12">
                            <div className='col-span-12 md:col-span-12 lg:col-span-12 '>

                            {isLoadingClients ?
                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            </div>
                            :
                            <div>
                                <p className='text-[12px] text-[#C1C1C1] '>Select Client</p>
                                <ReactSelect 
                                    defaultValue={selectedClient}
                                    onChange={setSelectedClient}
                                    formatOptionLabel={formatOptionLabel1}
                                    options={clients}
                                    />
                            </div>
                            }
                            </div>

                            {showPlansSelect ?

                            <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[5px]'>

                            {isLoadingPlans ?
                                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                    <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                </div>
                                :
                                <div>
                                <p className='text-[12px] text-[#C1C1C1]'>Select Plan</p>
                                
                                <ReactSelect 
                                    defaultValue={selectedPlan}
                                    onChange={setSelectedPlan}
                                    formatOptionLabel={formatOptionLabel}
                                    options={plans}    
                                    />
                                </div>
                            }
                                </div>
                            : <></>
                            }
                            
                        </div>
                    </form>
                </div>
    );
}