import React, { useEffect, useState } from 'react';
import Layout from '../components/layout';
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

import Public from './Components/public'
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../public/Dashhoard/youtube.svg'
import Counter from './Components/count';
import Accounts from './Components/AccountsStatus';

export default function Dashoard() {

  const [showDots, setShowDots] = useState(true);

  const [data, setData] = useState([ ]);
  const [isLoading, setIsLoading] = useState(false)


  return (
    <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-8 ml-[30px] mr-[30px] mt-[45px]'>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>
                    Dashoard
                  </div> 
              </div>
              
          </div >
          <div className="mt-[24px] text-[18px] font-semibold">
            Status
          </div>
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#0062E0] to-[#19AFFF] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[42px] font-semibold text-[#FFFFFF]">
                      <Counter/>
                    </text>
                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgFacebook}
                    layout='fixed'
                    alt='ImgFacebook'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#FCA759] via-[#E82D56] via-[#A22DB4] to-[#643DCE] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[42px] font-semibold text-[#FFFFFF]"><Counter/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgIg}
                    layout='fixed'
                    alt='ImgIg'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#161616] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[42px] font-semibold text-[#FFFFFF]"><Counter/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgTiktok}
                    layout='fixed'
                    alt='ImgTiktok'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#0CA8F6] to-[#0096E1] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[42px] font-semibold text-[#FFFFFF]"><Counter/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgTelegram}
                    layout='fixed'
                    alt='ImgTelegram'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#DB0505] to-[#FF0000] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[42px] font-semibold text-[#FFFFFF]"><Counter/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgYoutube}
                    layout='fixed'
                    alt='ImgYoutube'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-gradient-to-l from-[#FBBC04] to-[#FCD462] rounded-[16px] justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[42px] font-semibold text-[#FFFFFF]"><Counter/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[20px]'>
                  <Image
                    src={ImgGmail}
                    layout='fixed'
                    alt='ImgGmail'
                    width={48}
                    height={48}
                    
                  />
                
                </div>
            </div>
          </div>

          <div className="mt-[24px] text-[18px] font-semibold">
          Accounts status
          </div>
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[32px] font-semibold text-[#000000]"><Accounts/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#000000]">ACTIVE PLANS</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[32px] font-semibold text-[#000000]"><Accounts/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#000000]">PLANS TOTAL</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                
                </div>
            </div>
            <div className='col-span-12 md:col-span-12 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                <div>
                  <div className="flex flex-row">
                    <text className="text-[32px] font-semibold text-[#000000]"><Accounts/></text>
                          
                  </div>
                  <div className="text-[10px] text-[#000000]">ADS TOTAL</div>
                </div>
                <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                
                </div>
            </div>
          </div>

          <div className="mt-[24px] text-[18px] font-semibold">
          Ads overview
          </div>

          <div className='grid grid-cols-12 mt-[15px]'>
            <div className="col-span-12 md:col-span-12 lg:col-span-12 h-[200px] md:h-[330px] lg:h-[188px] bg-[#FFFFFF] rounded-[24px] pt-[10px] pr-[5px]">
                {/* <BarData/> */}

            </div>
          </div> 

        </div>
        <div className='col-span-12 md:col-span-12 lg:col-span-4'>
          <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12'>
              <Public/>
            </div>
          </div>

        </div>
      
      </div>
  )
}

Dashoard.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}
