import React, { useEffect, useState } from 'react';
import Layout from '../components/layout';
import TableDatosClients from './Components/tabladatos';
import ModalClient from "./Components/modalClient"
import CounterClients from './Components/counterClients';

export default function Clients() {
  const [showDots, setShowDots] = useState(true);

  const [reloadClients, setReloadClients] = useState(false);


  return (


    <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Clients</div>
                  <div className='flex flex-row '>
                    <ModalClient
                    reloadClients={reloadClients} setReloadClients={setReloadClients}/>
                  </div>
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px] gap-6'>

              <div className='col-span-12 md:col-span-12 lg:col-span-6'>

                <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Status</div>

                  <div className='grid grid-cols-12  gap-3 mt-[20px]'> 

                    <div className='col-span-12 md:col-span-4 lg:col-span-4 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] text-center" ><CounterClients/></div>
                      <div className="text-[12px] font-medium text-[#000000] text-center">Total</div>

                    </div>

                    <div className='col-span-12 md:col-span-4 lg:col-span-4 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] " ><CounterClients/></div>
                      <div className="text-[12px] font-medium text-[#000000] ">Active (With Plan)</div>
                      
                    </div>
                    <div className='col-span-12 md:col-span-4 lg:col-span-4 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                      <div className="text-[32px] font-semibold text-[#000000] " ><CounterClients/></div>
                      <div className="text-[12px] font-medium text-[#000000] ">New this month</div>
                      
                    </div>
                  
                  </div >
                  
              </div>

              <div className='col-span-12 md:col-span-12 lg:col-span-6'>

              <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>This month client overview by plan</div>

                <div className='grid grid-cols-12 mt-[20px]'>
                  
                  
  
                  <div className='col-span-12 md:col-span-12 lg:col-span-12'>

                    {/* <BarChartClient/> */}
  
  
                  </div>
                  
                </div >
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px] ml-[20px] mr-[20px]'>

              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                <TableDatosClients reloadClients={reloadClients} setReloadClients={setReloadClients}/>
  
              </div>
              
          </div >
        </div>
      </div>


  )
}

Clients.getLayout = function getLayout(page) {
  return (
    <Layout>{page}</Layout>
  )
}
