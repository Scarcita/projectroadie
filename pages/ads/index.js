import React, { useEffect, useState } from 'react';
import Layout from "../components/layout"
import Table from "./Components/tabladatos";
import ModalAd from "./Components/modalAds";
import Counter from './Components/counter';
import Image from "next/image";

import ImgFacebook from '../../public/Dashhoard/facebook.svg'
import ImgIg from '../../public/Dashhoard/instagram.svg'
import ImgMessenger from '../../public/Dashhoard/messenger.svg'
import ImgTelegram from '../../public/Dashhoard/telegram.svg'
import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
import ImgTwitter from '../../public/Dashhoard/twitter.svg'
import ImgWhatsapp from '../../public/Dashhoard/whatsapp.svg'
import ImgYoutube from '../../public/Dashhoard/youtube.svg'



export default function Ads() {

  const [reloadAds, setReloadAds] = useState(false);
  const [showDots, setShowDots] = useState(true);

  const [data, setData] = useState([ ]);

  return (


    <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Ads</div>
                  <div className='flex flex-row '>
                    <ModalAd reloadAds={reloadAds} setReloadAds={setReloadAds}  />
                  </div>
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px] gap-6'>

              <div className='col-span-12 md:col-span-12 lg:col-span-6'>

                <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Month overview</div>

                <div className='grid grid-cols-12 mt-[20px]'>
                  
                  
  
                  <div className='col-span-12 md:col-span-12 lg:col-span-12'>

                    {/* <Graficos/> */}
  
  
                  </div>
                  
                </div >
                  
              </div>

              <div className='col-span-12 md:col-span-12 lg:col-span-6'>

              <div className='text-[14px] md:text-[16px] lg:text-[17px] text-[#000000] font-semibold'>Month status</div>

              <div className="mt-[20px] grid grid-cols-12 gap-3">
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                        <Image
                            src={ImgFacebook}
                            layout='fixed'
                            alt='ImgFacebook'
                            width={56}
                            height={56}
                            
                        />
                        <div className='ml-[10px]'>
                        <div className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><Counter/></div>
                        <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                        </div>

                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                        <Image
                            src={ImgTelegram}
                            layout='fixed'
                            alt='ImgTelegram'
                            width={56}
                            height={56}
                            
                        />
                        <div className="ml-[10px]">
                            <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><Counter/></text>
                            <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                        </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                        <Image
                            src={ImgIg}
                            layout='fixed'
                            alt='ImgIg'
                            width={56}
                            height={56}
                            
                        />
                        <div className="ml-[10px]">
                            <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><Counter/></text>
                            <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                        </div>
                    
                </div>
                <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row'>
                        <Image
                            src={ImgYoutube}
                            layout='fixed'
                            alt='ImgYoutube'
                            width={56}
                            height={56}
                            
                        />
                        <div className="ml-[10px]">
                            <text className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]"><Counter/></text>
                            <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                        </div>
                </div>

            </div>
                  
              </div>
              
          </div >

          <div className='grid grid-cols-12 mt-[20px] ml-[20px] mr-[20px]'>

              <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                <Table/>
  
              </div>
              
          </div >
        </div>
      </div>
  )
}



Ads.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}
