import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import ReactSelect from 'react-select';
import Image from 'next/image';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


export default function ModalAds(props) {
    
    const { reloadAds, setReloadAds} = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [clientPlanPosts, setClientPlanPosts] = useState([])
    const [PlanPostId, setPlanPostId] = useState(null)
    const [subTitle, setSubTitle] = useState("");
    const [title, setTitle] = useState('')
    const [post, setPost] = useState('')
    const [rrss, setRrss] = useState('')
    const [type, setType] = useState('')
    const [clientPlanPostId, setClientPlanPostId] = useState('')
    const [startDate, setStartDate] = useState('')
    const [startTime, setStartTime] = useState('')
    const [endDate, setEndDate] = useState('')
    const [endTime, setEndTime] = useState('')
    const [amount, setAmount] = useState('')

    const clearForm = () => {
        setClientPlanPostId('')
        setStartDate('')
        setEndDate('')
        setAmount('')
    }
    

    const formatOptionLabel = ({ value, label, post }) => {

        const hasImage = false;
        if(post._matchingData.ClientsPlansPosts.media_url !== null && post._matchingData.ClientsPlansPosts.media_url.trim().length !== 0){
            hasImage = true;
        }

        return (
            <div className='grid grid-cols-12'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row mt-[5px] mb-[5px]'>
                    {hasImage ?
                        <div className='pr-[10px]'>
                            <Image
                                className='rounded-sm'
                                src={post._matchingData.ClientsPlansPosts.media_url}
                                alt=''
                                layout='fixed'
                                width={40}
                                height={40}
                            />
                        </div>
                        : <></>
                    }
                    <div className={ hasImage ? 'col-span-8' : 'col-span-12' + ' text-left'}>
                        {post._matchingData.ClientsPlansPosts.title}
                        <div>
                            <small className='text-gray-500 text-xs'>{post._matchingData.ClientsPlansPosts.subtitle}</small>
                        </div>
                    </div>
                </div>
            </div>
        )
    };      
    
    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clientsPlansPostsAds/all/')
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result._matchingData.ClientsPlansPosts.title, post: result})
                })

                setClientPlanPosts(temp)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

    }, [])
    
    const getDataOrden = async () => {
        var data = new FormData();
        console.warn(clientPlanPostId)
        console.warn(endDate)
        console.warn(startDate)
    
        data.append("client_plan_post_id", clientPlanPostId.value);
        data.append("start_datetime", startDate + ' ' + startTime);
        data.append("end_datetime", endDate + ' ' + endTime );
        data.append("amount", amount);
    
        fetch("https://slogan.com.bo/roadie/clientsPlansPostsAds/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            //console.log('VALOR ENDPOINTS: ', data);

            setIsLoading(false)
            if(data.status){
              clearForm()
              setReloadAds(!reloadAds)
              setShowModal(false)
              //IMPLEMENTAR RECARGA DE DATOS (COMPARTIR FUNCION DE PANTALLA AL MODAL)
            } else {
              alert(JSON.stringify(data.errors, null, 4))
            }
            
          },
          (error) => {
            console.log(error)
          }
          )
    
      }


    
    const validationSchema = Yup.object().shape({
        title: Yup.string()
        .required('title is required'),

        subTitle: Yup.string()
            .required("subtitle is required"),
        // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        post: Yup.string()
            .required('mediaUrl is required'),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        rrss: Yup.string()
            .required('rrss is required'),
            //.oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        type: Yup.string()
            .required('type is required'),
        //   //.min(6, 'minimo 6 caracteres'),
        // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),
    
        startDate: Yup.string()
            .required('startDate is required'),
    
        startTime: Yup.string()
        .required('startTime is required'),
    
        endTime: Yup.string()
            .required('endTime is required'),

        endDate: Yup.string()
        .required('endDate is required'),

        amount: Yup.string()
        .required('amount is required'),
    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            getDataOrden()
            return false;
            
        }


    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[22px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[46px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[12px] md:text-[15px] lg:text-[15px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Ads
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE ADS
                                    </h4>

                                    </div>
                                    <div>
                                        <form onSubmit={handleSubmit(onSubmit)}>

                                            <div className="mt-[20px] grid grid-cols-12 gap-4">

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Title</p>
                                                    <input name="title"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                    {...register('title')}
                                                    value={title}
                                                    onChange={(e) => {
                                                        setTitle(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                                                </div>
                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>SubTitle</p>
                                                    <input name="subTitle"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                    {...register('subTitle')}
                                                    value={subTitle}
                                                    onChange={(e) => {
                                                        setSubTitle(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.subTitle?.message}</div>
                                                </div>

                                                <div className='col-span-12 md:col-span-12 lg:col-span-6 '>
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Post</p>
                                                    <ReactSelect 
                                                        className="text-start"
                                                        defaultValue={PlanPostId}
                                                        onChange={setPlanPostId}
                                                        formatOptionLabel={formatOptionLabel}
                                                        options={clientPlanPosts} 
                                                    />
                                                </div>


                                                <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Red Social</p>
                                                    <select
                                                        name="rrss"
                                                        {...register('rrss')}
                                                        value={rrss}
                                                        onChange={(e) => {
                                                        setRrss(e.target.value);
                                                        }}
                                                        className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    >
                                                       <option value="Facebook">Facebook</option>
                                                        <option value="Instagram">Instagram</option>
                                                        <option value="Mailing">Mailing</option>
                                                        <option value="Youtube">Youtube</option>
                                                        <option value="Tiktok">Tiktok</option>
                                                        <option value="LinkedInd">LinkedIn</option>
                                                        <option value="Twitter">Twitter</option>
                                                    </select>
                                                    <div className="text-[14px] text-[#FF0000]">{errors.rrss?.message}</div>
                                                </div>

                                                <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Type</p>
                                                    <select
                                                        name="type"
                                                        {...register('type')}
                                                        value={type}
                                                        onChange={(e) => {
                                                        setType(e.target.value);
                                                        }}
                                                        className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    >
                                                        <option value="Image">Image</option>
                                                        <option value="Album">Album</option>
                                                        <option value="Video">Video</option>
                                                        <option value="Story">Story</option>
                                                        <option value="Reel">Reel</option>
                                                    </select>
                                                    <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                                                </div>

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>amount</p>
                                                    <input name='amount' type="number" placeholder="1.0" step="0.01" min="0.01"  className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                    {...register('amount')}
                                                    value={amount}
                                                    onChange={(e) => {
                                                        setAmount(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.amount?.message}</div>
                                                </div>

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date start </p>
                                                    <input name="startDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                    {...register('startDate')}
                                                    value={startDate}
                                                    onChange={(e) => {
                                                        setStartDate(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.startDate?.message}</div>
                                                </div>
                                                
                                                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time Star</p>
                                                    <input name="startTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                    {...register('startTime')}
                                                    value={startTime}
                                                    onChange={(e) => {
                                                        setStartTime(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.startTime?.message}</div>
                                                </div>

                                                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date End </p>
                                                    <input name="endDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                    {...register('endDate')}
                                                    value={endDate}
                                                    onChange={(e) => {
                                                        setEndDate(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.endDate?.message}</div>
                                                </div>
                                                
                                                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time End</p>
                                                    <input name="endTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                    {...register('endTime')}
                                                    value={endTime}
                                                    onChange={(e) => {
                                                        setEndTime(e.target.value)
                                                    }}
                                                    />
                                                    <div className="text-[14px] text-[#FF0000]">{errors.endTime?.message}</div>
                                                </div>
                                            
                                            </div>
                                            <div className="flex flex-row justify-between mt-[20px]">

                                                <div>
                                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                                </div>

                                                <div>
                                                    <button
                                                        className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#643DCE] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                        onClick={() =>
                                                            setShowModal(false)
                                                        }
                                                        disabled={isLoading}
                                                    >
                                                        Cancel
                                                    </button>
                                                    <button
                                                        className="w-[100px] h-[35px] border-[1px] bg-[#643DCE] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                        type="submit"
                                                        disabled={isLoading}
                                                    >
                                                        {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}