import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import { title } from 'process';



export default function ModalDetalle(props) {
  const {showModal, setShowModal, selectedPost, todaysDate} = props;


  console.log(todaysDate)

 
    return (
        <>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                <div>Posts Day</div>
                                <div className='col-span-12 md:col-span-12 lg:col-span-12  bg-[#fff] shadow-md rounded-[20px] p-[20px] mb-[15px]'
                                    key={selectedPost.id}>
                                    <div className="flex flex-row">
                                        <div className="w-[48px] h-[48px] rounded-full bg-[#643DCE]">
                                        {selectedPost.clients_plan.client.img_url != null && selectedPost.clients_plan.client.img_url.length > 0 ?
                                                
                                                <Image 
                                                className='rounded-full'
                                                src={selectedPost.clients_plan.client.img_url}
                                                alt='media'
                                                layout='responsive'
                                                // height={17}
                                                // width={17}
                                                >

                                                </Image> 
                                                : <></>  
                                            } 
                            
                                        </div>
                                        <div className="pl-[10px]">
                            
                                        {selectedPost.clients_plan.client.name !== null ?
                                            <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal'>
                                            {selectedPost.clients_plan.client.name}
                                            </p>
                                            : <></>
                                        }
                                        <p className='text-[12px]'>18-11-22</p>
                            
                                        </div>
                            
                                    </div>
                                    <div className='mt-[15px]'>
                                    {selectedPost.media_url != null && post.media_url > 0 ?
                                                
                                        <Image
                                        className='rounded-full'
                                        src={post.media_url}
                                        alt='media'
                                        layout='responsive'
                                        // height={37}
                                        // width={37}
                                        >
                                    </Image>
                                        : <></>  
                                    } 
                                    </div>
                                    {selectedPost.post_copy !== null && selectedPost.post_copy !== undefined ?
                                    <div className='mt-[5px] '>
                                        <p className='text-[12px] leading-4 whitespace-normal'>{selectedPost.post_copy}</p>
                                        
                                    </div>
                                    : <></>
                                    }
                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'> 

                                    <button className='w-full h-[48px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[18px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7]'>
                                        
                                        Edit post
                                    
                                    </button>
                                        
                                    </div>
                        
                                </div>
                                <div className='text-end'>
                                    <button
                                        className="w-[100px] h-[35px] border-[1px] border-[#582BE7] rounded-[30px] text-[#582BE7] hover:bg-[#582BE7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                        onClick={() =>
                                            setShowModal(false)
                                        }
                                    >
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}
