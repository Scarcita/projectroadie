import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';
import imagenFaceboock from "../../../public/Dashhoard/facebook.svg";


export default function ModalCreate(props) {

    const { reloadPosts, setReloadPosts, client_plan_id} = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingClients, setIsLoadingClients] = useState(true);
    const [isLoadingPlans, setIsLoadingPlans] = useState(true);

    const [showPlansSelect, setShowPlansSelect] = useState(false);
    const [showFormFields, setShowFormFields] = useState(false);

    const { reloadClients, setReloadClients, client_id } = props;

    const [time, setTime] = useState('')

    const [selectedClient, setSelectedClient] = useState(null);
    const [clients, setClients] = useState([]);

    const [selectedPlan, setSelectedPlan] = useState(null);
    const [plans, setPlans] = useState([]);

    // const [clientPlanId, setClientPlanId] = useState("");
    // const [clientPlan, setClientPlan] = useState("");

    const [socialNetwork, setSocialNetwork] = useState("");
    const [type, setType] = useState('a')
    const [plannedDateTime, setPlannedDateTime] = useState('')
    const [title, setTitle] = useState('')
    const [subtitle, setSubTitle] = useState('')
    const [instructions, setInstructions] = useState('')
    const [postCopy, setPostCopy] = useState('')
    const [mediaUrl, setMediaUrl] = useState('')

    const clearForm = () => {
        setSocialNetwork('')
        setType('')
        setPlannedDateTime('')
        setTitle('')
        setSubTitle('')
        setInstructions('')
        setPostCopy('')
        setMediaUrl('')
    }

    /////////////////////// SELECT NAME ///////////////////////

    const formatOptionLabel1 = ({ value, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>

                <div>
                    <small className='text-black text-xs'>{post.name}</small>
                </div>
            </div>
        )
    }; 

    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clients/all')
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.name, post: result})
                })

                setClients(temp)
                //console.log('DATA CLIENTS: ', temp);
                
            } else {
                console.error(data.error)
            }
            setIsLoadingClients(false)
        })

    }, [])

    /////////////////////////////////SELECT PLAN///////////////////////////////////////

    const formatOptionLabel = ({ row, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>

                <div>
                    <small className='text-gray-500 text-xs'>{post.plan.name}</small>
                </div>
            </div>
        )
    }; 

    useEffect(() => {
        if(selectedClient){
            setIsLoadingPlans(true)
            setShowPlansSelect(true)
            setShowFormFields(false)
            fetch('https://slogan.com.bo/roadie/clientsPlans/all/' + selectedClient.value)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
    
                    var temp = [];
                    data.data.map((row) => {
                        temp.push({value: row.id, label: row.plan.name, post: row})
                    })
    
                    setPlans(temp)
                } else {
                    console.error(data.error)
                }
                setIsLoadingPlans(false)
            })
        }
    }, [selectedClient])


    useEffect(() => {
        if(selectedPlan){
            setShowFormFields(true)
        }
    }, [selectedPlan])


   
    const getDataOrden = async () => {
        var data = new FormData();
        data.append("client_id", selectedClient.value);
        data.append("client_plan_id", selectedPlan.value);
        data.append("title", title);
        data.append("subtitle", subtitle);
        data.append("social_network", socialNetwork);
        data.append("type", type);
        data.append("planned_datetime", plannedDateTime);
        data.append("instructions", instructions);
        data.append("post_copy", postCopy);
        data.append("media_url", mediaUrl);

    
        fetch("http://slogan.com.bo/roadie/clientsPlansPosts/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
              //console.log('VALOR ENDPOINTS: ', data);
              setIsLoading(false)
              if(data.status){
                clearForm()
                //setReloadClients(!reloadClients)
                setReloadPosts(!reloadPosts)
                setShowModal(false)
                //IMPLEMENTAR RECARGA DE DATOS (COMPARTIR FUNCION DE PANTALLA AL MODAL)
              }
              
            },
            (error) => {
              //console.log(error)
            }
          )
    
      }
    

          
    const validationSchema = Yup.object().shape({
        // clientPlanId: Yup.string()
        // .required('clientPlanId is required'),
    
        type: Yup.string()
            .required('type is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
        title: Yup.string()
        .required('title is required'),

        subtitle: Yup.string()
            .required("subtitle is required"),
    
        socialNetwork: Yup.string()
            .required('socialNetwork is required')
            .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        type: Yup.string()
            .required('type is required'),
        //   //.min(6, 'minimo 6 caracteres'),
        // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),
    
        plannedDateTime: Yup.string()
            .required('plannedDateTime is required'),
    
        instructions: Yup.string()
            .required('instructions is required'),
    
        postCopy: Yup.string()
            .required('postCopy is required'),

        time: Yup.string()
        .required('time is required'),

    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            //console.log('string is NOT empty')
            getDataOrden()
            return false;
            
        }

    return (
        <>
            <div>
                <button
                    className="w-[120px] h-[32px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[48px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[14px] md:text-[18px] lg:text-[18px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create post
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE POST
                                    </h4>

                                </div>

                                <div>
                                    <form onSubmit={handleSubmit(onSubmit)}>

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                           
                                            <div className='col-span-12 md:col-span-12 lg:col-span-12 '>

                                            {isLoadingClients ?
                                            
                                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                                <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                            </div>
                                            :
                                                <div>
                                                    <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Select Name</p>
                                                    <ReactSelect 
                                                        defaultValue={selectedClient}
                                                        onChange={setSelectedClient}
                                                        formatOptionLabel={formatOptionLabel1}
                                                        options={clients}
                                                        />
                                                </div>
                                            }
                                            </div>
                                            

                                            {showPlansSelect ?
                                             
                                                <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                                                {isLoadingPlans ?
                                                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                                    <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                                </div>
                                                :
                                                    <div>
                                                        <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Select Plan</p>
                                                        
                                                        <ReactSelect 
                                                            defaultValue={selectedPlan}
                                                            onChange={setSelectedPlan}
                                                            formatOptionLabel={formatOptionLabel}
                                                            options={plans}    
                                                            />
                                                    </div>
                                                    }
                                                </div>
                                            : <></>}

                                            {showFormFields ? 
                                                <>
                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Title</p>
                                                        <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                        {...register('title')}
                                                        value={title}
                                                        onChange={(e) => {
                                                            setTitle(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                                                    </div>
                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>SubTitle</p>
                                                        <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                        {...register('subtitle')}
                                                        value={subtitle}
                                                        onChange={(e) => {
                                                            setSubTitle(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.subtitle?.message}</div>
                                                    </div>
                                                    <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Red Social</p>
                                                        <select
                                                            {...register('socialNetwork')}
                                                            name="socialNetwork"
                                                            value={socialNetwork}
                                                            onChange={(e) => {
                                                            setSocialNetwork(e.target.value);
                                                            }}
                                                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                        >
                                                            <option value="Facebook">Facebook</option>
                                                            <option value="Instagram">Instagram</option>
                                                            <option value="Youtube">Youtube</option>
                                                            <option value="Mailing">Mailing</option>
                                                            <option value="TikTok">TikTok</option>
                                                            <option value="LinkedIn">LinkedIn</option>
                                                            <option value="Twitter">Twitter</option>
                                                        </select>
                                                        <div className="text-[14px] text-[#FF0000]">{errors.socialNetwork?.message}</div>
                                                    </div>

                                                    <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Type</p>
                                                        <select
                                                            {...register('type')}
                                                            name="type"
                                                            value={type}
                                                            onChange={(e) => {
                                                            setType(e.target.value);
                                                            }}
                                                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                        >
                                                            <option value="Image">Image</option>
                                                            <option value="Album">Album</option>
                                                            <option value="Video">Video</option>
                                                            <option value="Story">Story</option>
                                                            <option value="Reel">Reel</option>

                                                        </select>
                                                        <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                                                    </div>

                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date</p>
                                                        <input name="plannedDateTime" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                        {...register('plannedDateTime')}
                                                        value={plannedDateTime}
                                                        onChange={(e) => {
                                                            setPlannedDateTime(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.plannedDateTime?.message}</div>
                                                    </div>
                                                    
                                                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time</p>
                                                        <input name="time"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                        {...register('time')}
                                                        value={time}
                                                        onChange={(e) => {
                                                            setTime(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.time?.message}</div>
                                                    </div>
                                                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Image</p>
                                                        <input name="mediaUrl"  type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px]'
                                                        {...register('mediaUrl')}
                                                        value={mediaUrl}
                                                        onChange={(e) => {
                                                            setMediaUrl(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                                                    </div>

                                                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Indications</p>
                                                        <input name="instructions"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                        {...register('instructions')}
                                                        value={instructions}
                                                        onChange={(e) => {
                                                            setInstructions(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.instructions?.message}</div>
                                                    </div>
                                                    <div className="col-span-12 md:col-span-6 lg:col-span-12">
                                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Post_copy</p>
                                                        <input name="tepostCopyxt"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                        {...register('postCopy')}
                                                        value={postCopy}
                                                        onChange={(e) => {
                                                            setPostCopy(e.target.value)
                                                        }}
                                                        />
                                                        <div className="text-[14px] text-[#FF0000]">{errors.postCopy?.message}</div>
                                                    </div>
                                                </>
                                            : <></>}
                                            
                                        </div>
                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                    type="submit"
                                                    disabled={isLoading}
                                                >
                                                   {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}