import React, { useEffect, useState } from 'react';
import Layout from '../components/layout';
import Anuncios from "./Components/anuncios"
import GraficCalendar from "./Components/graficCalendar"
import ModalCreate from "./Components/Modal"
import Counter from './Components/counter';
import Client from './Components/clientsActive';
import ModalDetalle from './Components/modalDetalle';



export default function Calendar(props) {
  return (
      <div className='grid grid-cols-12'>
        <div className='col-span-12 md:col-span-12 lg:col-span-8 ml-[30px] mr-[30px] mt-[45px]'>
          <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                  <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Calendar</div>
                  <div className=''>
                    <ModalCreate />
                  </div>
                  
              </div>
              
          </div >
          <div className="mt-[24px] text-[18px] font-semibold">
            Status
          </div>
            <div className='grid grid-cols-12 gap-3 mt-[24px]'>
              
                <div className='col-span-12 md:col-span-6 lg:col-span-6 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>
                  <div className='w-[full] justify-center text-center ml-[30px] mr-[30px]'><Client/></div>
                  <div className="text-[12px] font-medium text-[#000000] ">clients active</div>
                </div>
                  
                  
                <div className='col-span-6 md:col-span-3 lg:col-span-3 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                  <div className="text-[32px] font-semibold text-[#000000] text-center" ><Counter/></div>
                  <div className="text-[12px] font-medium text-[#000000] text-center">Scheduled for today</div>

                </div>

                <div className='col-span-6 md:col-span-3 lg:col-span-3 h-[120px] bg-[#F3F3F3] rounded-[17px] justify-center text-center pt-[20px]'>

                  <div className="text-[32px] font-semibold text-[#000000] " ><Counter/></div>
                  <div className="text-[12px] font-medium text-[#000000] ">Posted this week</div>
                  
                </div>
              
            </div >

            <div className='grid grid-cols-12 mt-[35px]'>
            
              <div className='col-span-12 md:col-span-12 lg:col-span-12 pb-[30px]'>
                <GraficCalendar/>

              </div>

          </div>
        </div>
        <div className='h-screen col-span-12 md:col-span-12 lg:col-span-4'>
          <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12'>
              <Anuncios/>
            </div>
          </div>

        </div>
      </div>
  )
}

Calendar.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}
